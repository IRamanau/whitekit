package com.example.transformer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class XmlTransformerTest {
    private static final Logger LOGGER = LogManager.getLogger(XmlTransformerTest.class.getName());
    private static final String XML_INPUT = "<List>" +
            "<item>" +
            "<id>1</id>" +
            "<tariff>velcom</tariff>" +
            "<balance>131</balance> " +
            "</item>" +
            "<item>" +
            "<id>2</id>" +
            "<tariff>velcom</tariff>" +
            "<balance>42</balance> " +
            "</item>" +
            "</List>";
    private final static String XML_FILE_PATH = "src/test/resources/transformer/input.xml";
    private final static String XSL_FILE_PATH = "src/test/resources/transformer/transform.xsl";
    private final static String RESULT_FILE_PATH = "src/test/resources/transformer/output.xml";
    private XmlTransformer xmlTransformer = new XmlTransformer();
    private static List<String> list = new ArrayList<>();
    @Test
    public void transform() {
        list.add("1/131");
        list.add("2/42");
        File result = new File(RESULT_FILE_PATH);
        xmlTransformer.transform(XML_INPUT, XML_FILE_PATH, XSL_FILE_PATH, RESULT_FILE_PATH);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document document = builder.parse(result);
            document.getDocumentElement().normalize();
            assertEquals(document.getDocumentElement().getNodeName(), "LIST");
            NodeList nodeList = document.getElementsByTagName("balance");
            for(int i = 0; i< nodeList.getLength(); i++){
                assertEquals(nodeList.item(i).getTextContent(), list.get(i));
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            LOGGER.error(e.getMessage());
        }


    }
}