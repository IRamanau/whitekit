package com.example.parser;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CustomXMLParserTest {
    private static final String XML_PATH = "src/test/resources/parser/output.xml";
    CustomXMLParser xmlParser = new CustomXMLParser();
    List<String > list = new ArrayList<>();
    @Test
    public void parse() {
        list.add("1/131");
        list.add("2/42");
        assertEquals(xmlParser.parse(XML_PATH),list);
    }
}