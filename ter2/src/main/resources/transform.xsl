<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
        xmlns:xsl=
                "http://www.w3.org/1999/XSL/Transform"
        version="1.0"
>
    <xsl:template match="List">
        <LIST>
            <xsl:for-each select="item">
                <balance>
                    <xsl:value-of select="id"/>/<xsl:value-of select="balance"/>
                </balance>
            </xsl:for-each>
        </LIST>
    </xsl:template>
</xsl:stylesheet>