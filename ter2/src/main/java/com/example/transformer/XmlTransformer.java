package com.example.transformer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Class XmlTransformer need to transformer from input xml to new xml using transformer.xsl
 */
public class XmlTransformer {
    private static final Logger LOGGER = LogManager.getLogger(XmlTransformer.class.getName());

    /**
     * Constructor without params
     */
    public XmlTransformer() {
    }

    /**
     * Method need to convert from xml to new xml
     * @param inputData - input data, we need to translate
     * @param inputXmlPath - path to intermediate file we keep inputData
     * @param xslPath - path to xsl file
     * @param outputXmlPath - path to new xml file
     */
    public void transform(String inputData,String inputXmlPath, String xslPath, String outputXmlPath ) {
        try {
            Files.write(Paths.get(inputXmlPath) ,inputData.getBytes());
            TransformerFactory factory = TransformerFactory.newInstance();
            Source xsltFile = new StreamSource(new File(xslPath));
            Transformer transformer = factory.newTransformer(xsltFile);
            LOGGER.info(inputData);
            Source xmlFile = new StreamSource(new File(inputXmlPath));
            transformer.transform(xmlFile,new StreamResult(new File(outputXmlPath)));
        } catch (TransformerException | IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
