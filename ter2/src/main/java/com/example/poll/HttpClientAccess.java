package com.example.poll;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class HttpClientAccess need to poll resource from 1st app
 */
public class HttpClientAccess{
    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private static final Logger LOGGER = LogManager.getLogger(HttpClientAccess.class.getName());

    /**
     * Constructor without params
     */
    public HttpClientAccess() {
    }

    /**
     * Method startToPoll need to start poll to resource
     * @throws Exception - any catch exception
     */
    public String startToPoll() throws Exception{
        HttpClientAccess obj = new HttpClientAccess();
        try {
            return obj.sendGet();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }finally {
            obj.close();
        }
        return null;
    }

    private void close() throws IOException {
        httpClient.close();
    }

    private String sendGet() throws Exception {
        HttpGet request = new HttpGet("http://localhost:8083/getAll");
        try (CloseableHttpResponse response = httpClient.execute(request)) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String result = EntityUtils.toString(entity);
                    return result;
                }
        }
        return null;
    }
}
