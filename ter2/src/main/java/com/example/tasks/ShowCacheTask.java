package com.example.tasks;

import com.example.amqp.Sender;
import com.example.dao.CouchbaseDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Class ShowCacheTask need to run logic that try get data from db and send by AMQP broker to RabbitMQ
 */
public class ShowCacheTask implements Runnable{
    //This constant need for check, which data need to require
    private static final Integer BALANCE_TO_NEED = 100;
    //This constant need for check, when to show info from 1st app
    private static final long TIME_TO_CHECK = 10;
    private static final Logger LOGGER = LogManager.getLogger(ShowCacheTask.class.getName());
    private static CouchbaseDAO dao = new CouchbaseDAO();
    private static Sender sender = new Sender();

    /**
     * Method run provide a endless loop that get data from DB and send to RabbitMQ by AMQP broker
     */
    @Override
    public void run() {
        try {
            while (true){
                LOGGER.info("------------------------------");
                Map<String, Object> map = dao.getDataFromDB();
                Map<String, Object> newMap = new HashMap<>();
                map.entrySet()
                        .stream()
                        .filter(entry -> Integer.valueOf(entry.getValue().toString())<BALANCE_TO_NEED)
                        .forEach(e -> newMap.put(e.getKey(), e.getValue()));
                sender.send(newMap);
                TimeUnit.SECONDS.sleep(TIME_TO_CHECK);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
