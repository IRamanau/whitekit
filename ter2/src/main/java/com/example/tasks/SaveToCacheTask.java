package com.example.tasks;

import com.example.dao.CouchbaseDAO;
import com.example.poll.HttpClientAccess;
import com.example.transformer.XmlTransformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

/**
 * Class SaveToCacheTask need to run a on every 5 seconds poll 1st app
 * transformer xml to new xml by xslt
 * and save to DB
 */
public class SaveToCacheTask implements Runnable{

    //This constant need for check, when to update info from 1st app
    private static final long TIME_TO_CHECK = 5;
    //
    private final static String XML_FILE_PATH = "src/main/resources/input.xml";
    private final static String XSL_FILE_PATH = "src/main/resources/transform.xsl";
    private final static String RESULT_FILE_PATH = "src/main/resources/output.xml";
    private static final Logger LOGGER = LogManager.getLogger(SaveToCacheTask.class.getName());
    private static HttpClientAccess httpClientAccess = new HttpClientAccess();
    private static CouchbaseDAO dao = new CouchbaseDAO();

    /**
     * Constructor without params
     */
    public SaveToCacheTask() {
    }

    /**
     * Method run provide a endless loop where we need poll mapping, receive xml and send to DB
     */
    @Override
    public void run() {
        XmlTransformer xmlTransformer = new XmlTransformer();
        try {
            while (true){
                LOGGER.info("========================================");
                String xml = httpClientAccess.startToPoll();
                xmlTransformer.transform(xml, XML_FILE_PATH, XSL_FILE_PATH, RESULT_FILE_PATH);
                dao.saveXmlToDB(RESULT_FILE_PATH);
                TimeUnit.SECONDS.sleep(TIME_TO_CHECK);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
