package com.example;

import com.example.tasks.SaveToCacheTask;
import com.example.tasks.ShowCacheTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Runner
 */
public class Runner {

    private final static int COUNT_OF_THREADS = 2;
    private static SaveToCacheTask saveToCacheTask = new SaveToCacheTask();
    private static ShowCacheTask showCacheTask = new ShowCacheTask();

    /**
     * method main run 2 threads
     * @param args - args
     */
    public static void main(String[] args) {
        ExecutorService executorService =
                Executors.newFixedThreadPool(COUNT_OF_THREADS);
        executorService.execute(saveToCacheTask);
        executorService.execute(showCacheTask);

    }

}
