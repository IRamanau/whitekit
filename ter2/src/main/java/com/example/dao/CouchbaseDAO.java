package com.example.dao;


import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.example.parser.CustomXMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Class CouchbaseDAO need to work with Couchbase like send or receive info
 */
public class CouchbaseDAO {
    //Constants
    private static final String COUCHBASE_LOGIN = "user" ;
    private static final String COUCHBASE_PASSWORD = "user1111" ;
    private static final String COUCHBASE_BUCKET_NAME = "user_info" ;
    private static final Logger LOGGER = LogManager.getLogger(CouchbaseDAO.class.getName());
    private static CustomXMLParser xmlParser = new CustomXMLParser();

    /**
     * Method need to save data from xml file to database
     * @param xmlPath - path to xml file
     */
    public void saveXmlToDB(String xmlPath) {
        List<String> xmlDataList = xmlParser.parse(xmlPath);
        xmlDataList.forEach(LOGGER::info);
        saveToDB(splitter(xmlDataList));
    }

    private void saveToDB(Map<String, String> input) {
        Cluster cluster = CouchbaseCluster.create("localhost");
        Bucket bucket = cluster
                .authenticate(COUCHBASE_LOGIN, COUCHBASE_PASSWORD)
                .openBucket(COUCHBASE_BUCKET_NAME);
        JsonObject person = JsonObject.create();
        input.entrySet().stream().forEach(data -> person.put(data.getKey(),data.getValue()));
        JsonDocument document = JsonDocument.create("balance", person);
        bucket.upsert(document);
    }

    private Map<String, String> splitter(List<String> input){
        Map<String, String> map = new HashMap<>();
        for(int i = 0; i < input.size(); i ++){
            String[] strArray = input.get(i).split("/");
            map.put(strArray[0], strArray[1]);
        }
        map.forEach(LOGGER::info);
        return map;
    }

    /**
     * Method getDataFromDB need to receive data from db
     * @return - content from db like a map
     */
    public Map<String, Object> getDataFromDB() {
        Cluster cluster = CouchbaseCluster.create("localhost");
        Bucket bucket = cluster
                .authenticate(COUCHBASE_LOGIN, COUCHBASE_PASSWORD)
                .openBucket(COUCHBASE_BUCKET_NAME);
        JsonDocument result = bucket.get("balance");
        return result.content().toMap();
    }
}
