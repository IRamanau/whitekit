package com.example.amqp;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Map;

/**
 * Class Sender need to send data to AMQP broker
 */
public class Sender {
    private final static String QUEUE_NAME = "user_info";
    private static final Logger LOGGER = LogManager.getLogger(Sender.class.getName());

    /**
     * Method send need to send info to amqp broker RabbitMQ
     * @param map - map with K - id, V - balance of user
     * @throws Exception - exception
     */
    public void send(Map<String , Object> map) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            map.forEach((key, value) -> {
                try {
                    channel
                            .basicPublish("", QUEUE_NAME, null,
                                    value.toString().getBytes());
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage());
                }
            });
        }
    }
}
