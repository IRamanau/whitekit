package com.example.ter.controller;


import com.example.ter.config.PersistenceConfig;
import com.example.ter.model.UserInfo;
import com.example.ter.repo.UserInfoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.*;

import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration

@ContextConfiguration(classes = { PersistenceConfig.class})
public class MainControllerTest {


    private MockMvc mockMvc;
    @Autowired
    private UserInfoRepository repository;
    @Test
    public void getAll() throws Exception {
        List<UserInfo> userInfoList = new ArrayList<UserInfo>();
        userInfoList.add(new UserInfo("test1", 100));
        userInfoList.add(new UserInfo("test2", 101));
        when(repository.findAll()).thenReturn(userInfoList);

        mockMvc.perform(get("/getAll"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(IntegrationTestUtil.APPLICATION_XML_UTF8));
        verify(repository, times(1)).findAll();
    }
}
