package com.example.ter.service;

import com.example.ter.model.UserInfo;
import com.example.ter.repo.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Class {@link UserInfoService} need to provide business logic
 */
@Service
public class UserInfoService {

    @Autowired
    UserInfoRepository repository;

    /**
     * Method findAll need to return all UserInfo
     * @return - lists of UserInfo
     */
    public List<UserInfo> findAll(){
        return repository.findAll();
    }
}
