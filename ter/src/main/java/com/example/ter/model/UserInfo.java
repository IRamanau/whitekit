package com.example.ter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Class UserInfo - model class
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tariff;
    private int balance;

    /**
     * Constructor with 2 params
     * @param tariff - tariff of mobile operator
     * @param balance - user balance
     */
    public UserInfo(String tariff, int balance) {
        this.tariff = tariff;
        this.balance = balance;
    }
}
