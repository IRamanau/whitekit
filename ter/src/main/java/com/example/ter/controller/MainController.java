package com.example.ter.controller;

import com.example.ter.model.UserInfo;
import com.example.ter.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class need to provide user info to view part of application
 */
@RestController()
public class MainController {
    @Autowired
    UserInfoService service;

    /**
     * Method getAll need to return all user info in json or xml
     * @return list of user info
     */
    @GetMapping(value = "/getAll",produces = {MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE })
    List<UserInfo> getAll(){
        return service.findAll();
    }
}
